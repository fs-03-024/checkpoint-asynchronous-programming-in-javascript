async function iterateWithAsyncAwait(values) {
    for (const value of values) {
      console.log(value);
      await delay(1000); // Attendre 1 seconde avant de passer à la valeur suivante
    }
  }
  
  function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }