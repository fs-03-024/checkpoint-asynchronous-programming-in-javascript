async function parallelCalls(urls) {
    const promises = urls.map(url => fetch(url)); // Crée un tableau de promesses pour chaque URL
  
    try {
      const responses = await Promise.all(promises); // Attend que toutes les promesses se terminent
      const data = await Promise.all(responses.map(response => response.json())); // Attend la conversion en JSON de chaque réponse
  
      console.log('Responses:', data);
    } catch (error) {
      console.error('Error:', error);
    }
  }