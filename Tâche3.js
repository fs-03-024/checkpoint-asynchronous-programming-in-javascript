async function concurrentRequests() {
    try {
      const request1 = fetch('https://api.example.com/data1'); // Première demande d'API
      const request2 = fetch('https://api.example.com/data2'); // Deuxième demande d'API
  
      const [response1, response2] = await Promise.all([request1, request2]); // Attend la résolution de deux demandes simultanément
  
      const data1 = await response1.json(); // Récupère les données JSON de la première réponse
      const data2 = await response2.json(); // Récupère les données JSON de la deuxième réponse
  
      console.log('Combined results:', [data1, data2]);
    } catch (error) {
      console.error('An error occurred:', error);
    }
  }